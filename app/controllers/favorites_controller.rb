class FavoritesController < ApplicationController

  def create
    begin
      create_record
      flash[:notice] = "success like"
    rescue
      flash[:danger] = "failed like"
    end
    redirect_back(fallback_location: root_path)
  end
  
  def destroy
    begin
      destroy_record
      flash[:notice] = "success unlike"
    rescue
      flash[:danger] = "faild unlike"
    end
    redirect_back(fallback_location: root_path)
  end
  
  private
    def favorite_params
      params.require(:favorites).permit(:user_id, :photo_id)
    end
    
    def notification_params
      { reciever_id: @photo.user_id, sender_id: params[:favorites][:user_id],
        photo_id: params[:favorites][:photo_id], action: "favorite" }
    end
    
    def create_record
      @favorite = Favorite.new(favorite_params)
      @photo = Photo.find(params[:favorites][:photo_id])
      @notification = Notification.new(notification_params)
      ActiveRecord::Base.transaction do
        if Notification.where(notification_params).present?
          raise "already exist record"
        end
        @favorite.save!
        @notification.save!
      end
    end
    
    def destroy_record
      @favorite = Favorite.find_by(favorite_params)
      @photo = Photo.find(params[:favorites][:photo_id])
      @notification = Notification.find_by(notification_params)
      ActiveRecord::Base.transaction do
        @favorite.destroy!
        @notification.destroy!
      end
    end
end
