class SessionsController < ApplicationController

  def create
    user = User.find_by(user_name: params[:session][:user_name])
    if user && user.authenticate(params[:session][:password])
      log_in(user)
      redirect_to user
    else
      flash[:danger] = "invalid user_name password combination"
      redirect_to root_path
    end
  end
  
  def delete
    log_out
    redirect_to root_path
  end

end
