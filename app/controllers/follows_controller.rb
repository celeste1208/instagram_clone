class FollowsController < ApplicationController
  def create
    begin
      create_record
      flash[:notice] = "success follow"
    rescue
      flash[:danger] = "failed follow"
    end
    redirect_to user_path(params[:follows][:followed_user_id])
  end
  
  def destroy
    begin
      destroy_record
      flash[:notice] = "success unfollow"
    rescue
      flash[:danger] = "failed unfollow"
    end
    redirect_to user_path(params[:follows][:followed_user_id])
  end
  
  private
    def follow_params
      params.require(:follows).permit(:user_id, :followed_user_id)
    end
    
    def notification_params
      { reciever_id: params[:follows][:followed_user_id],
        sender_id: params[:follows][:user_id], action: "follow" }
    end
    
    def create_record
      @follow = Follow.new(follow_params)
      @notification = Notification.new(notification_params)
      ActiveRecord::Base.transaction do
        if Notification.where(notification_params).present?
          raise "already exist record"
        end
        @follow.save!
        @notification.save!
      end
    end
    
    def destroy_record
      @follow = Follow.find_by(follow_params)
      @notification = Notification.find_by(notification_params)
      ActiveRecord::Base.transaction do
        @follow.destroy!
        @notification.destroy!
      end
    end
end
