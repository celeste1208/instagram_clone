class TopController < ApplicationController

	def index
		if logged_in
			@photos = get_follows_photos
		end
	end
	
	def get_follows_photos
		Photo.where(user_id: get_follows_ids).order(created_at: "DESC")
	end
	
	def get_follows_ids
		Follow.where(user_id: session[:user_id]).pluck(:followed_user_id)
	end

end
