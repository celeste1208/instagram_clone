class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, 
    :edit_password, :update_password]

  # GET /users
  # GET /users.json
  def index
    if params[:search]
      if params[:search] == ""
        @search_users = []
      else
        @search_users = User.where('user_name like ?', "%#{params[:search]}%")
      end
    end
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @photos = @user.photos
    @follow_count = @user.follows.size
    @follower_count = @user.followeds.size
    @is_followed_by_current_user = @user.followeds.where(user_id: session[:user_id]).present?
  end

  # GET /users/new
  def new 
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end
  
  def edit_password
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params_create)

    respond_to do |format|
      if @user.save
        log_in(@user)
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params_update)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_password
    if validates_password
      if @user.update(password: params[:user][:new_password]) 
        flash[:notice] = "successs change password"
        redirect_to user_path
      else
        flash[:danger] = "failed to change password by DB error"
        redirect_to edit_password_user_path
      end
    else
      redirect_to edit_password_user_path
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    log_out
    flash[:notice] = "User was successfully destroyed."
    redirect_to root_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def user_params_create
      params.require(:user).permit(:name, :user_name, :email, :password, :password_confirmation, :image, :provider, :uid)
    end
    
    def user_params_update
      params.require(:user).permit(:name, :user_name, :web_site, :description, :email, :tel, :sex)
    end
    
    def validates_password 
      if !@user.authenticate(params[:user][:password])
        flash[:danger] = "incorrect password"
        return
      end
      
      if params[:user][:new_password].empty?
        flash[:danger] = "can not blank"
        return
      end
      
      if @user.authenticate(params[:user][:new_password])
        flash[:danger] = "以前のパスワードと同じです。"
        return
      end
      
      if params[:user][:new_password] != params[:user][:new_password_confirmation]
        flash[:danger] = "not match new password"
        return
      end
      
      return true
    end
    
end