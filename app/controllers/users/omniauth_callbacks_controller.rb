class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    # You need to implement the method below in your model (e.g. app/models/user.rb)
    @user = User.from_omniauth(request.env["omniauth.auth"])

    if @user.persisted?
      log_in(@user)
      flash[:notice] = "Success to LogIn by Facebook"
      redirect_to user_path(@user.id)
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      render template: "users/new"
    end
  end

  def failure
    redirect_to root_path
  end
end