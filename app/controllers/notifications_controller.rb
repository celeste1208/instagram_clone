class NotificationsController < ApplicationController
  def index
    ActiveRecord::Base.transaction do
      @new_notifications = Notification.where.not(sender_id: current_user).where(
        reciever_id: current_user, checked: false).order(id: "DESC")
      @new_notifications.each do |notification|
        notification.update!(checked: true)
      end
    end
    @notifications = Notification.where.not(sender_id: current_user).where(
      reciever_id: current_user).order(id: "DESC")
  end
end
