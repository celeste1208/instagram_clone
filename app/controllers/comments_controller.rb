class CommentsController < ApplicationController
  def create
    begin
      create_record
      flash[:notice] = "success add comment"
    rescue
      flash[:danger] = "failed add comment"
    end
    redirect_back(fallback_location: root_path)
  end
  
  private 
    def comment_params
      params.require(:comment).permit(:content, :user_id, :photo_id)
    end
    
    def notification_params
      { reciever_id: @comment.photo.user_id, sender_id: params[:comment][:user_id],
        photo_id: params[:comment][:photo_id], comment_id: @comment.id, action: "comment" }
    end
    
    def create_record
      ActiveRecord::Base.transaction do
        @comment = Comment.new(comment_params)
        @comment.save!
        @notification = Notification.new(notification_params)
        @notification.save!
      end
    end
end
