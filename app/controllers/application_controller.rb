class ApplicationController < ActionController::Base
  include SessionsHelper
  include NotificationsHelper
  protect_from_forgery with: :exception
end
