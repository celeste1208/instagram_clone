json.extract! user, :id, :name, :user_name, :web_site, :description, :email, :tel, :sex, :password, :created_at, :updated_at
json.url user_url(user, format: :json)
