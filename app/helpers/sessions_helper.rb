module SessionsHelper

  def log_in(user)
    session[:user_id] = user.id
  end

  def log_out
    session.delete(:user_id)
  end
  
  def logged_in
    current_user
  end
  
  def current_user
    session[:user_id]
  end
  
  def is_owner(user_id)
    user_id == session[:user_id]
  end
  
end
