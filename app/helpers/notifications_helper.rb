module NotificationsHelper
  def has_notification
    Notification.where.not(sender_id: current_user).where(
        reciever_id: current_user, checked: false).present?
  end
end
