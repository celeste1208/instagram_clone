class Notification < ApplicationRecord
  belongs_to :reciever, class_name: "User"
  belongs_to :sender, class_name: "User"
  belongs_to :photo, optional: true
  belongs_to :comment, optional: true
  
  validates :reciever_id, presence: true
  validates :sender_id, presence: true
  validates :action, presence: true
end
