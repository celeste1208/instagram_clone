class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :photo
  has_many :notification, dependent: :destroy
  
  validates :content, presence: true
  validates :user_id, presence: true
  validates :photo_id, presence: true
end
