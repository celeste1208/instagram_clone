class Photo < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy
  has_many :favorites, dependent: :destroy
  has_many :notification, dependent: :destroy
  mount_uploader :content, PhotoUploader
  
  validates :content, presence: true
  validates :user_id, presence: true
  
  def is_favorite_by(favorite_by)
    self.favorites.where(user_id: favorite_by).present?
  end
  
  def get_favorite_count
    self.favorites.count
  end
  
end
