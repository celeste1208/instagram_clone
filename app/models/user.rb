class User < ApplicationRecord
  devise :omniauthable
  
  has_many :photos,  dependent: :destroy
  has_many :follows, class_name: "Follow", foreign_key: "user_id", dependent: :destroy
  has_many :followeds, class_name: "Follow", foreign_key: "followed_user_id", dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :favorites, dependent: :destroy
  has_many :recieve_notification, class_name: "Notification", foreign_key: "reciever_id", dependent: :destroy
  has_many :send_notifification, class_name: "Notification", foreign_key: "sender_id", dependent: :destroy

  validates :name, presence: true
  validates :user_name, presence: true, uniqueness: true
  
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, on: :create
  
  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_initialize do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.name = auth.info.name   # assuming the user model has a name
      user.image = auth.info.image # assuming the user model has an image
      # If you are using confirmable and the provider(s) you use validate emails, 
      # uncomment the line below to skip the confirmation emails.
      # user.skip_confirmation!
    end
  end
  
end
