class CreateNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications do |t|
      t.integer :reciever_id, null: false
      t.integer :sender_id, null: false
      t.integer :photo_id
      t.integer :comment_id
      t.string :action, null: false
      t.boolean :checked, default: false, null: false

      t.timestamps
      
      t.references :reciever, foreign_key: {to_table: :users}
      t.references :sender, foreign_key: {to_table: :users}
    end
  end
end
