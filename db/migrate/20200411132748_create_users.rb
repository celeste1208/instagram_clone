class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :user_name
      t.string :web_site
      t.string :description
      t.string :email
      t.string :tel
      t.string :sex
      t.string :password

      t.timestamps
    end
  end
end
