class AddIndexFavoritesUserId < ActiveRecord::Migration[5.1]
  def change
    add_index :favorites, [:user_id, :photo_id], unique: true
  end
end
