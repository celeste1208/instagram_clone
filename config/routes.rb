Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'top#index'

  resources :users do
    member do
      get :edit_password 
      patch :update_password
    end
  end
  
  resources :photos, only: [:index, :create, :show, :new]
  resource :follows, only: [:create, :destroy]
  resource :comments, only: [:create]
  resource :favorites, only: [:create, :destroy] 

  resources :notifications, only: [:index]
  resources :guidelines, only: [:index]
  
  post '/login', to: 'sessions#create'
  get '/logout', to: 'sessions#delete'
  
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }

end
